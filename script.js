$('document').ready(function() {

console.log("Yup");

$(".background").fadeShow({

// the aspect fill will be intact even when resizing the window

//only handy for full width / height slideshows, otherwise slows your page down
correctRatio: true,
// the slides will be shuffled before shown, get a unique slider each refresh
shuffle: true,
// milliseconds per slide
speed: 5000,
// images (urls) to create the slideshow with, array of strings
images: ['https://s3.amazonaws.com/seydisigns/dk2.jpg', 'https://s3.amazonaws.com/seydisigns/dk3.jpg','https://s3.amazonaws.com/seydisigns/bos2.jpg']
 
});

window.sr = ScrollReveal({ duration: 2000 });
sr.reveal('.about');
sr.reveal('.pf');
sr.reveal('#contactbox');

// Code to animate scrollspy
$(".navbar a").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    }  // End if
  });

$('.grid').isotope({
  // options
  itemSelector: '.grid-item',
  layoutMode: 'fitRows'
});

// init Isotope
var $grid = $('.grid').isotope({
  // options
});
// filter items on button click
$('.filter-button-group').on( 'click', 'button', function() {
  var filterValue = $(this).attr('data-filter');
  $grid.isotope({ filter: filterValue });
});

// Isotope Filters
$grid.isotope({ filter: '.veurk' });
$grid.isotope({ filter: '.persoproj' });
$grid.isotope({ filter: '.diploma' });
$grid.isotope({ filter: '*' });


// Resume descriptions
$('.pftext').hide(); //hide description div until hover

$('.grid-item').hover(
function(){
// $(this).find('.pftext').addClass('animated fadeInDown');
$(this).find('.pftext').show();
},
function(){
// $(this).find('.pftext').addClass('animated fadeOutUp');
$(this).find('.pftext').hide();
});

// $('.filterbut').click(function(){
// $(this).css('border', 'none');
// });

canSubmit();

// $('.grid-item').hover(
// function(){
// $(this).find('.pftext').addClass('animated fadeInDown');
// $(this).css('height', '270px');
// $(this).css('width', '270px');
// $(this).find('.pftext').css('height', '270px');
// $(this).find('.pftext').css('width', '270px');
// },
// function(){
// $(this).find('.pftext').addClass('animated fadeOutUp');
// $(this).css('height', '250px');
// $(this).css('width', '250px');
// $(this).find('.pftext').css('height', '250px');
// $(this).find('.pftext').css('width', '250px');
// });
});

// To make sure the email address has the proper format
function validateEmail(email) 
{
  var regex = /\S+@\S+\.\S+/;
  return regex.test(email);
};

// Return true if all the requied fields are filled and the email
// is in the right format. Return false otherwise
function canSubmit(){
  var x = document.forms["mainform"]["message"];
  var z = document.forms["mainform"]["email"];
  if(x.value =="" || z.value==""){
    return false;
  }
  return validateEmail(z.value);
};